let moves = 0;

$(function () {
    refresh();
    showBox();
    movesCount();

    //dragged element in field
    $('.box-item').click(function () {
        let clickedDiv = $(this);
        let nextElement = clickedDiv.next();
        let prevElement = clickedDiv.prev();
        let clearItem = $('.clear-item');
        let difference = clickedDiv.index() - clearItem.index();
        //dragged clear element to up or down
        if (difference == 4 || difference == -4) {
            updateClearItem(clickedDiv, clearItem);
            moves++;
            movesCount();
        }
        //dragged clear element to right
        if (nextElement.hasClass('clear-item')) {
            if (clearItem.index() % 4 == 0) {
                return false;
            }
            clickedDiv.insertAfter(clearItem);
            moves++;
            movesCount();
        }
        //dragged clear element to left
        if (prevElement.hasClass('clear-item')) {
            if (clickedDiv.index() % 4 == 0) {
                return false;
            }
            clickedDiv.insertBefore(clearItem);
            moves++;
            movesCount();
        }
        if (checkResult()) $('.game-over').show();
    });

    //show popup
    $('.show-popup').click(function () {
        $('.refresh').show();
    })

    //working popup
    $('.alert-button').click(function () {
        if ($(this).val() == 'ok') refresh();
        $('.alert-window').hide();
    })

    //working array item
    function checkResult() {
        let arr = $('.box-item');
        for (let i = 0; i < arr.length - 1; i++) {
            if (arr[i].textContent != (i + 1)) {
                return false;
            }
        }
        return true
    }
});

//dragged up or down clear item
function updateClearItem(clicked, clear) {
    let textContent = clicked.text();
    clear.removeClass("clear-item").text(textContent);
    clicked.addClass("clear-item").text('');
}

//render count moves
function movesCount() {
    let movesCount = $('.moves');
    movesCount.html('Количество ходов:' + moves);
}

//refresh game field
function refresh() {
    let boxContainer = $(".box");
    let divs = boxContainer.children();
    while (divs.length) {
        boxContainer.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
    }
    moves = 0;
    movesCount();
}

function showBox() {
    $('.box').animate({height: "toggle", opacity: "1"}, 2000);
    $('h1').animate({fontSize: "30px"}, 1000);
    $('.show-popup').animate({opacity: "1"}, 2000);
}